//
//  TermVC.m
//  OurdrumHarry
//
//  Created by Amrita on 18/12/15.
//  Copyright © 2015 Digicrazers. All rights reserved.
//

#import "TermVC.h"

@interface TermVC ()<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *myWeb;

@end

@implementation TermVC

- (void)viewDidLoad {
    [super viewDidLoad];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] init];
    barButton.title = @"Back";
    self.navigationController.navigationBar.topItem.backBarButtonItem = barButton;
    [self.myWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://ourdrum.com/pages/terms-conditions.html"]]];
    // Do any additional setup after loading the view.
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
